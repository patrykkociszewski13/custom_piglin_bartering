package net.sssubtlety.custom_piglin_bartering.mixin_helpers;

import net.fabricmc.fabric.api.tag.TagRegistry;
import net.minecraft.item.Item;
import net.minecraft.loot.LootTable;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import java.util.HashMap;
import java.util.List;

public interface PiglinBrainMixinAccessor {
    HashMap<Item, LootTable> barters = new HashMap<>();

    static void buildBarters(MinecraftServer server) {
        System.out.println("CustomPiglinBartering: building barters");
        barters.clear();
        List<Item> barterItems = TagRegistry.item(new Identifier("piglib:piglin_bartering_items")).values();
        Identifier itemId;
        LootTable table;
        for (Item item : barterItems) {
            itemId = Registry.ITEM.getId(item);
            table = server.getLootManager().getTable(new Identifier("custom_piglin_bartering:" + itemId.getNamespace() + "/" + itemId.getPath()));
            barters.put(item, table);
        }
    }

    static HashMap<Item, LootTable> getBarters() {
        return barters;
    }
}
