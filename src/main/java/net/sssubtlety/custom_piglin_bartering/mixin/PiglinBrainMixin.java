package net.sssubtlety.custom_piglin_bartering.mixin;


import net.minecraft.entity.mob.PiglinBrain;
import net.minecraft.entity.mob.PiglinEntity;
import net.minecraft.item.*;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.context.LootContextParameters;
import net.minecraft.loot.context.LootContextTypes;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Hand;
import net.sssubtlety.custom_piglin_bartering.mixin_helpers.PiglinBrainMixinAccessor;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import java.util.List;

@Mixin(PiglinBrain.class)
public class PiglinBrainMixin implements PiglinBrainMixinAccessor {
//	private static final Item unmatchableItem = new Item(new Item.Settings());

	/**
	 * @author supersaiyansubtlety
	 * I am conceptually replacing acceptsForBarter's behavior. I do my own checks to see if
	 * the item is accepted and if it is store it in the piglin's offhand so my getBarteredItem
	 * overwrite can read it.
	 * The new method may return different values than the old method and the old method must not run
	 */
	@Redirect(method = "consumeOffHandItem", at = @At(value = "INVOKE", target =
			"Lnet/minecraft/entity/mob/PiglinBrain;acceptsForBarter(Lnet/minecraft/item/Item;)Z"))
	private static boolean consumeOffHandItemAcceptsForBarter(Item item, PiglinEntity piglin) {
		boolean accepted = isAccepted(item);
		//piglin's offHand is re-set to item so the item is available for getBarteredItem
		if(accepted) { piglin.setStackInHand(Hand.OFF_HAND, new ItemStack(item)); }
		return accepted;
	}

//	@Redirect(method = "canGather", at = @At(value = "INVOKE", target =
//			"Lnet/minecraft/entity/mob/PiglinBrain;acceptsForBarter(Lnet/minecraft/item/Item;)Z"))
//	private static boolean canGatherAcceptsForBarter(Item item, PiglinEntity piglin) {
//		return isAccepted(item);
//	}

//	@Redirect(method = "method_27086", at = @At(value = "INVOKE", target =
//			"Lnet/minecraft/entity/mob/PiglinBrain;acceptsForBarter(Lnet/minecraft/item/Item;)Z"))
//	private static boolean method_27086GatherAcceptsForBarter(Item item, PiglinEntity piglin) {
//		return isAccepted(item);
//	}

	/**
	 * @author supersaiyansubtlety
	 * @reason I need to completely replace the logic of acceptsForBarter so that I can check if
	 * the passed item is part of a custom list loaded from a tag.
	 */
//	@Overwrite
//	private static boolean acceptsForBarter(Item item) {
//		return isAccepted(item);
//	}
	/**
	 * @author supersaiyansubtlety
	 * @reason This replaces getBartedItem with a version that gives different items for
	 * different input items. Input items are retreived from the piglin's offhand,
	 * which has been set by customAcceptsForBarter.
	 */
	@Overwrite
	private static List<ItemStack> getBarteredItem(PiglinEntity piglin) {
//		System.out.println("CustomPiglinBartering: overwriting getBarteredItem. ");

		/**piglin's offhand (set by customAcceptsForBarter) is stored
		 * and then re-set to empty.
		 */
		ItemStack itemStack = piglin.getStackInHand(Hand.OFF_HAND);
		piglin.setStackInHand(Hand.OFF_HAND, ItemStack.EMPTY);
		LootTable barterLoot = PiglinBrainMixinAccessor.getBarters().get(itemStack.getItem());
		return getBarterStack(barterLoot, piglin);
	}

//	@Redirect(method = "canGather", at = @At(value = "FIELD", opcode = Opcodes.GETSTATIC,
//			target = "Lnet/minecraft/item/Items;GOLD_NUGGET:Lnet/minecraft/item/Item;"))
//	private static Item goldNuggetRedirect () {
//		Item temp = unmatchableItem;
//		return unmatchableItem;
//	}

//	@Redirect(method = "loot", at = @At(value = "FIELD", opcode = Opcodes.GETSTATIC,
//			target = "Lnet/minecraft/item/Items;GOLD_NUGGET:Lnet/minecraft/item/Item;"))
//	private static Item goldNuggetRedirect2 () {
//		Item temp = unmatchableItem;
//		return unmatchableItem;
//	}



	private static List<ItemStack> getBarterStack(LootTable table, PiglinEntity piglin) {
		List<ItemStack> list = table.generateLoot((new LootContext.Builder((ServerWorld)piglin.world)).parameter(LootContextParameters.THIS_ENTITY, piglin).random(piglin.world.random).build(LootContextTypes.BARTER));
		return list;
	}

	private static boolean isAccepted(Item item) {
		return PiglinBrainMixinAccessor.getBarters().keySet().contains(item);
	}
}


