- 1.2.1 (18 Jan. 2021): Updated embeded piglib version, fixing server crash.  
- 1.2 (16 Jan. 2021): Marked as compatible with 1.16.5.
- 1.1-beta (27 Dec. 2020): Now bundles [piglib](https://www.curseforge.com/minecraft/mc-mods/piglib) for better compatibility. 
  If you've made a custom datapack, just rename+move your `data/custom_piglin_bartering/tags/items/barter_items.json` to
  `data/piglib/tags/items/piglin_bartering_items.json` and you're set. 
  Also compatible with newer Fabric API versions. 
- 1.0.6 (3 Nov. 2020): Marked as compatible with 1.16.4, replaced one mixin with a Fabric Api event. 
- 1.0.5 (18 Sep. 2020): Marked as compatible with 1.16.3. 
