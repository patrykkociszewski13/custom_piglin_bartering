Minecraft mod for the [Fabric mod loader](fabricmc.net) and Minecraft 1.16. 

Allows datapacks to:
1) ~~define a list of items that piglins are attracted to~~ removed because this is now handled by the base game's `piglin_loved` item tag
2) define a list of items that piglins will barter for
3) map each barterable item to a loot table that represents possible bartering returns

By default it separates the vanilla barters into 3 tiers mapped to gold nuggets, ingots, and blocks.

If you'd like more details on the project, you can follow the project wiki linked above, or you can follow this link to go straight to datapack formatting. 

The latest version is for all 1.16 release versions, remember to install the correct version of the Fabric API. The link below will take you to all the 1.16 versions of the Fabric API.

This is made for use in modpacks, no need to ask permission.

This mod is only required server-side.

![[Fabric API](https://www.curseforge.com/minecraft/mc-mods/fabric-api/files/all?filter-game-version=1738749986:70886)](https://i.imgur.com/Ol1Tcf8.png)

This mod is only for Fabric and I won't be porting it to Forge. The license is [MIT](https://will-lucic.mit-license.org/), however, so anyone else is free to port it.

I'd appreciate links back to this page if you port or otherwise modify this project, but links aren't required.
